//React core
import React from "react";

//Components
import Card from "../molecules/Card";
import Header from "../organisims/Header";

export default function HomePage({information}) {
    //We render multiple componentsusing map function
    const Cards = information.map((item) => {
        return <Card key={item.id} data={item} />;
      });
    return (
        <div className="home-page">
            <Header />
            <section className="recommended">
                <h2>Recommended videos</h2>
                <div className="grid">{Cards}</div>
                
            </section>
        </div>
        
    );

}