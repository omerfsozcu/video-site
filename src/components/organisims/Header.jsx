//React core
import React from "react";
import { Link } from "react-router-dom";

//Media assets
import logoLight from "../../assets/images/logo-light.svg";
export default function Header() {
    return (
        <header className="header">
            <Link to="/">
                <img src={logoLight} alt="Logo" />
            </Link>

            <div className="search-bar">
                <input placeholder="Search" />
                {/*Later willl be search*/}
                <Link  to="/">Search</Link>
            </div>
        </header>
    );
}
